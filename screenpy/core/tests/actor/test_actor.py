import pytest
from unittest.mock import MagicMock

from ..interactions.draw_picture import DrawPicture
from screenpy.core.actor import get_step_name, get_step_description


@pytest.fixture
def actor():
    actor = MagicMock()
    actor.name = "james"
    return actor


def test_generate_step_name_adds_correct_spacing(actor):
    camel = "MyLittleSentence"
    no_first = "myLittleSentence"

    assert get_step_name(actor, camel) == "james attempts to my little sentence"
    assert get_step_name(actor, no_first) == "james attempts to my little sentence"


def test_step_description_defaults_to_class_name():
    assert get_step_description(DrawPicture.titled("work of art")) == "DrawPicture"


def test_step_description_reads_description_override():
    class Interaction:
        def __init__(self):
            self.description = "interact with something else"

        def perform_as(self, actor):
            pass

    assert get_step_description(Interaction()) == "interact with something else"


def test_can_change_step_verb(actor):
    assert get_step_name(actor, "something", verb="changes") == "james changes something"


def test_step_name_handles_symbols(actor):
    assert get_step_name(actor, "view random.website.co.uk/slash?=yes") == "james attempts to view random.website.co.uk/slash?=yes"
