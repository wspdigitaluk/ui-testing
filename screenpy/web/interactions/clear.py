from selenium.webdriver.common.by import By

from ..abilities.browse_the_web import BrowseTheWeb
from ..helper import save_allure_screenshot_using


class Clear:
    def __init__(self, locator, strategy=By.CSS_SELECTOR):
        self.description = "clear text from element located by {}: {}".format(strategy, locator)
        self.locator = locator
        self.strategy = strategy

    @classmethod
    def the(cls, locator):
        try: # assume conforms to locator interface
            return cls(locator.locator, locator.strategy)
        except AttributeError:
            return cls(locator)

    def perform_as(self, actor):
        actor.ability_to(BrowseTheWeb).driver.find_element(self.strategy, self.locator).clear()
        save_allure_screenshot_using(actor)
