from screenpy.web.abilities.browse_the_web import BrowseTheWeb
from ..helper import save_allure_screenshot_using


class URL:
    def __init__(self):
        self.description = "the URL of the page"

    def answered_by(self, actor):
        save_allure_screenshot_using(actor)
        return actor.ability_to(BrowseTheWeb).driver.current_url
